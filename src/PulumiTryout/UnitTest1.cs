using AppGates.Net;
using AppGates.Net.Extensions.Core.ExplicitExtensions;
using AppGates.Net.Extensions.ExplicitExtensions;
using AppGates.Net.Grpc.Common.Services.Expression;
using AppGates.Net.Pulumi.Testing.Model;
using NUnit.Framework;
using Pulumi;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace PulumiTryout
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
            //var resourceGroup = new ResourceGroup("MyResourceGroup");
            //resourceGroup.
        }

        [Test]
        public void SetupCredentials()
        {
            var parametersToSet = new (string ParameterName, string EnvName, bool Secret)[]{
                ("azure:clientId", "AzureClientId",false),
                ("azure:clientSecret", "AzureClientSecret",true),
                ("azure:tenantId", "AzureTenantId",false),
                ("azure:subscriptionId", "AzureSubscriptionId",false),
                };

            foreach (var parameter in parametersToSet)
            {
                "pulumi".ToProcessStartInfo(
                    $"config set {parameter.ParameterName} {System.Environment.GetEnvironmentVariable(parameter.EnvName).SurroundWithQuotes()} {(parameter.Secret ? "--secret" : "")}")
                   .WithOutputRedirection(Console.Error.WriteLine, Console.Out.WriteLine)
                    .StartAndWait();

            }
        }

        private class PulumiAutomation
        {

            public PulumiAutomation(DirectoryInfo deployAppDirectory)
            {
                DeployAppDirectory = deployAppDirectory;
            }

            public async Task TestOperationAsync<TStack>(PulumiOperation operation,
                Func<ExpressionService<TStack>.Client,Task> testAsync)
                           where TStack : Stack<TStack>, new()
            {
                using (this.RunOperation<TStack>(operation).ToWaitForExitDisposable(
                    Debugger.IsAttached ? 20.Minutes() : 10.Minutes()))
                {
                    await using (var client = new ExpressionService<TStack>.Client().Connect())
                    {
                        await testAsync((ExpressionService<TStack>.Client)client);
                    }
                }
            }

            public Process RunOperation<TStack>(PulumiOperation operation)
                where TStack:Stack<TStack>,new()
            {
                "pulumi".ToProcessStartInfo("stack select dev")
      .WithOutputRedirection(Console.Error.WriteLine, Console.Out.WriteLine)
      .WithWorkingDirectory(DeployAppDirectory)
      .StartAndWait();

                var config = PulumiTestingConfiguration.FromType(typeof(MyStack), nameof(MyStack.RunAsync));
                config.TestProcessId = Process.GetCurrentProcess().Id;
                config.DryRun = false;
                config.Operation = operation;
                //config.SkipPreview = true;
                var json = config.Serialize();


                var arguments = config.Operation.GetDescription();
                if (config.SkipPreview && config.Operation != PulumiOperation.Preview)
                {
                    arguments += " --skip-preview";
                }

                arguments += " --non-interactive";

                DeployAppDirectory.CombineFile("Configuration.json").WriteContent(json);

                var pulumiDeploy = "pulumi".ToProcessStartInfo(arguments)
                       .WithOutputRedirection(Console.Error.WriteLine, Console.Out.WriteLine)
                       .WithWorkingDirectory(DeployAppDirectory)
                       .Start();
                return pulumiDeploy;
            }

            public DirectoryInfo DeployAppDirectory { get; }
        }

        [TestCase(PulumiOperation.Preview)]
        [TestCase(PulumiOperation.Up)]
        [TestCase(PulumiOperation.Destroy)]
        public async Task MyStackAutomation(PulumiOperation operation)
        {
            var deployAppDir = @"C:\git\appgates\Playground.BlazorStaticPage\src\Demo.Pulumi.DeploymentApp".ToDirectoryInfo();

            var automation = new PulumiAutomation(deployAppDir);
            await automation.TestOperationAsync<MyStack>(operation,
                async client=>
                {
                    var x = await client.Invoker.InvokeAsync(
                        m => m.ConnectionString.Task().As(x => x));
                    if(operation == PulumiOperation.Preview)
                    {
                        Assert.IsNull(x);
                    }
                    else
                    {
                        Assert.IsNotEmpty(x);
                    }
                });
        }
    }

}