﻿using AppGates.Net.Serialization;
using System;
using System.ComponentModel;
using System.Diagnostics;

namespace AppGates.Net.Pulumi.Testing.Model
{
    public class PulumiTestingResult
    {
        public byte[] ValidationResult { get; set; }
        public int ExitCode { get; set; }
    }
    public class PulumiTestingConfiguration : IJsonSerializable
    {
        static PulumiTestingConfiguration()
        {
            var settings =  default(PulumiTestingConfiguration).DefaultSerializationSettings()
                .WithSettings(true, false, true); // includeDefaults has a bug
            settings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Include;
            settings.DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Include;
        }

        public static PulumiTestingConfiguration FromType(Type stackType, string runMethodName)
        {
            var config = new PulumiTestingConfiguration()
            {
                StackAssemblyPath = stackType.Assembly.File().FullName,
                StackInitializerAssemblyQualifiedTypeName = stackType.AssemblyQualifiedName,
                StackInitializerMethodName = runMethodName,
            };
            return config;
        }
        public string StackAssemblyPath { get; set; }

        public string StackInitializerAssemblyQualifiedTypeName { get; set; } 
        public  string StackInitializerMethodName { get; set; }
        public bool AttachDebugger { get; set; } = Debugger.IsAttached;
        
        public bool DryRun { get; set; }

        public int TestProcessId { get; set; }

        public PulumiOperation Operation { get; set; }
        public bool SkipPreview { get; set; }
    }

    public enum PulumiOperation
    {
        [Description("up --yes")]
        Up,
        [Description("preview")]
        Preview,
        [Description("destroy --yes")]
        Destroy,
    }
}
