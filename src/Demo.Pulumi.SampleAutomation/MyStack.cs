using Pulumi;
using Pulumi.Azure.Core;
using Pulumi.Azure.Storage;
using System.Threading.Tasks;



using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AppGates.Net.ExpressionInvocation;
using System.Reflection;
using System.Diagnostics;
using AppGates.Net;

public static class ProcessExtensions
{
    public static IDisposable ToWaitForExitDisposable(this Process process, TimeSpan? timeout = null)
    {
        return Api.Create.Disposable(() => process?.WaitForExit(timeout, true, true));
    }
}
public static class StackAutomationExtensions
{
  
    public  async static Task<TConverted> As<T, TConverted>(this Task<T> task, Func<T, TConverted> converter)
    {
        return converter(await task);
    }



    public static Task<T> Task<T>(this Output<T> output)
    {
        return (Task<T>)output.GetType()
            .GetMethod("GetValueAsync", BindingFlags.Instance | BindingFlags.NonPublic)
            .Invoke(output, new object[] { });
    }
    public static T WaitUntil<T>(this Func<T> query, Func<T, bool> condition, TimeSpan timeout)
    {
        T result = default;
        new Func<bool>(() => condition(result = query())).WaitUntilTrue(timeout);
        return result;
    }
}


public class Stack<TSelf> : Stack
    where TSelf : Stack<TSelf>, new()
{
    public static async Task<int> RunAsync()
    {

        var result = await Deployment.RunAsync<TSelf>();
        var  x = MyStack.Stack.ConnectionString;

        return result;
    }
}
public class MyStack : Stack<MyStack>
{
    internal static MyStack Stack;
    public MyStack()
    {
        Stack = this;

        // Create an Azure Resource Group
        var resourceGroup = new ResourceGroup("resourceGroup");

        // Create an Azure Storage Account
        var storageAccount = new Account("storage", new AccountArgs
        {
            ResourceGroupName = resourceGroup.Name,
            AccountReplicationType = "LRS",
            AccountTier = "Standard"
        });

        // Export the connection string for the storage account
        this.ConnectionString = storageAccount.PrimaryConnectionString;

    }

    [Output]
    public Output<string> ConnectionString { get; set; }
}
