﻿using Pulumi;
using Pulumi.Azure.Core;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Azure = Pulumi.Azure;
namespace Demo.Pulumi.SampleAutomation
{
    class MongoDbStack:Stack<MongoDbStack>
    {
        public MongoDbStack()
        {
            var resourceGroup = new ResourceGroup("resourceGroup");
            var account = new Azure.CosmosDB.Account("CosmosAcccount", new Azure.CosmosDB.AccountArgs
            {
                ResourceGroupName = resourceGroup.Name
            });
            
            var exampleMongoDatabase = new Azure.CosmosDB.MongoDatabase("MyMongoDatabase", new Azure.CosmosDB.MongoDatabaseArgs
            {
                ResourceGroupName = resourceGroup.Name,
                AccountName = account.Name,
                Throughput = 400,
            });

            this.DatabaseConnection = exampleMongoDatabase.Id;
        }

        [Output]
        public Output<string> DatabaseConnection { get; }
    }
}
