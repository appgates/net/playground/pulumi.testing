﻿using AppGates.Net;
using AppGates.Net.Extensions.Core.ExplicitExtensions;
using AppGates.Net.Extensions.ExplicitExtensions;
using AppGates.Net.Grpc.Common.Services.Duplex;
using AppGates.Net.Grpc.Common.Services.Expression;
using AppGates.Net.Pulumi.Testing.Model;
using AppGates.Net.VisualStudio.DebuggerAutomation;
using Pulumi;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Demo.Pulumi.DeploymentApp
{
    class Program
    {
        static async Task<int> Main(params string[] args)
        {
            var config = new PulumiTestingConfiguration().Populate(
            typeof(Program).Assembly.GetResourceFileContent("Configuration.json"));
            if (config.AttachDebugger)
            {
                var debugManager = new DebugManager();
                debugManager.AttachVisualStudio(
                    Process.GetProcessById(config.TestProcessId), Process.GetCurrentProcess());
            }

            var dryRun = bool.Parse(Environment.GetEnvironmentVariable("PULUMI_DRY_RUN"));
            if(config.DryRun == true && dryRun == false)
            {
                return 0;
            }


            var stackAssemblyFile = config.StackAssemblyPath.ToFileInfo();
            AppDomain.CurrentDomain.ResolveAssembliesFrom(stackAssemblyFile.Directory());
            var stackType = Type.GetType(config.StackInitializerAssemblyQualifiedTypeName);
            var stackTypeAndBaseTypes = stackType.To().Enumerable().Concat(stackType.BaseTypes());


            var stackTask = (Task<int>)stackTypeAndBaseTypes.Select(t => t.GetMethod(
                 config.StackInitializerMethodName, BindingFlags.Static | BindingFlags.Public))
                    .First(m => m != null, $"Method '{config.StackInitializerMethodName}' on type '{stackType.FullName}' not found.")
                    .Invoke(null, null);

            var stack = Deployment.Instance.GetField<object>("_deployment").Get().GetField<Stack>("_stack").Get();
            if (stack == null)
            {
                throw Api.Create.Exception("Stack not set.");
            }

            await using (var server = new ExpressionService<Stack>.Service(stack).Bind())
            {
                //var result = 
                //config.Result = new PulumiTestingResult
                //{
                //    ExitCode = result
                //};

                await server.To().EventContainer().WaitForEventAsync<Duplex<byte[]>.ServerEndpoint>(
                         x => x.Source.ClientDisconnecting += x.Handler,
                         x => x.Source.ClientDisconnecting -= x.Handler);

                return await stackTask;
            }
        }

    }
}
